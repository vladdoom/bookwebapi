﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookWebApi_v2.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
namespace BookWebApi_v2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        BookRepository repo = new BookRepository();
        
        public BookController() { }

        // GET: api/Book
        [HttpGet]
        public IEnumerable<string> Get()
        {
              
            return new string[] { repo.GetBookList().ToString() };
              
        }

        // GET: api/Book/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return repo.GetBook(id).ToString();
        }

        // POST: api/Book
        [HttpPost]
        public void Post([FromBody] string value)
        {
            repo.Create(new Models.Book(0,"new", "new"));
        }

        // PUT: api/Book/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
            repo.Update(new Models.Book(0, "Update", "Update"));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            repo.Delete(id);
        }
    }
}
