﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookWebApi_v2.Models
{
    public class Book
    {
        public Book(long Id,string  Title, string Author) {
            this.Id = Id;
            this.Title = Title;
            this.Author = Author;
        }
        public long Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
    }
}
