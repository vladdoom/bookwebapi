﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookWebApi_v2.Models;

namespace BookWebApi_v2.Repository
{
    public class BookRepository : IRepositoryBook
    {
        InMemoryRepository repo = new InMemoryRepository();

        public void Create(Book item)
        {
            repo.CreateBook();

        }

        public void Delete(int id)
        {
            repo.DeleteBook();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Book GetBook(int id)
        {
          return  repo.getBook(id);

        }

        public IEnumerable<Book> GetBookList()
        {

          return  repo.GetBooks();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(Book item)
        {
            repo.PutBook();
        }
    }
}
