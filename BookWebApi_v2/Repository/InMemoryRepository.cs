﻿using BookWebApi_v2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookWebApi_v2.Repository
{
    public class InMemoryRepository
    {
        int i = 1;
        public List<Book> list { get; set; }
        public InMemoryRepository() {
            list.Add(new Book(i, "Sheva", "Net"));
        }
        public Book getBookInside() {
            return new Book(i, "Sova", "SQL");
        }

        public List<Book> GetBooks() {
            return list;
        }
        public Book getBook(int id) {
            return list.ElementAt(id);
        }
        public void CreateBook() {
            list.Add(getBookInside());
            i++;
        }
        public void PutBook() {
           Book book = list.ElementAt(i);
            book.Author = "Stiv";
            book.Title = "core";
        }
        public void DeleteBook() {
            list.ElementAtOrDefault(i);
            i--;
        }
    }
}
